# 1. Gestion de projet et documentation

Cette section présente les différentes étapes qui permettent de mener à bien la documentation des différents projets.


## Création compte Gitlab

Jusque là, rien de bien compliqué. Notre professeur nous envoie un mail détaillés des étapes successives qui nous permettent de créer un compte GitLab.
#### Qu'est-ce que git?
La documentation de projet représnte au moins la moitié de la quantité de travail pour ce cours de PHYS-F-517. Il est donc important de ne pas la négliger et de mettre sa documentation à jour régulièrement. 
Brièvement, Git est un outil de versionning, c'est-à-dire un outil qui donne accès à l'historique des modifications d'un document. Il existe bien sur plusieurs outils de versionning, mais l'avantage de Git est qu'il fonctionne avec un système de branches. On peut voir les branches comme un espace de projet parallèle dans lequel il est possible d'apporter des modifications au projet sans pour autant modifier l'espace de projet principal. 
Pour une meilleure compréhension sur les concepts informatiques difficilement tangibles pour les non-initiés, je vous conseille de regarder la série de vidéos faites par Grafikart.fr qui illustrent admirablement le fonctionnement de Git. [Cliquez ici pour accéder à la première vidéo de la série](https://www.youtube.com/watch?v=rP3T0Ee6pLU)
##  Installation virtual Box et obtention terminal (optionnel)
 Avant d'entamer l'installation de Git, je me suis demandé s'il n'existait pas un système d'exploitation dont les lignes de commandes était plus simples que celles sur Windows. Sur conseil d'un de mes camarades de classe, j'ai opté pour l'installation d'une machine virtuelle, afin d'avoir Linux comme système d'exploitation. Pour faire simple, j'ai téléchargé Vitual Box, la machine virtuelle, qui a son propre processeur, sa propre mémoire et sa propre interface sur mon ordinateur physique. Ensuite j'ai installé Ubuntu, mon système d'exploitation Linux qui contient un terminal et enfin je l'ai ouvert dans Virtual Box. 

Je mets ci-dessous le lien vers une vidéo qui explique très bien les différentes étapes pour installer Virtual Box.
[Cliquez ici.](https://www.youtube.com/watch?v=hYaCCpvjsEY)



## Installation Git
 
 Une fois que vous avez accès à votre terminal, vous pouvez entamer l'installation de Git.
 
 Tout d'abord, entrez cette commande dans votre treminal pour installer git : 

 ```
sudo apt-get install git
```
Pour vérifier que git est bien installé, entrez la commande
```
git --help 
```
.
Cette commande vous pemettra par ailleurs d'accéder à la liste de toutes les commandes git. Voici ci-dessous ce que vous devriez voir en entrant cette commande (sur Linux)

![Alt text](./images/githelp-min.png)

Une fois git installé, entrez
```
git config --global user.name "your_username"
```
puis
```
git config --global user.email "your_email_address@example.com"
```
en remplacant le contenu entre guillemet respectivement par votre identifiant personnel et par votre adresse e-mail afin de configurer votre git.



## Clé SSH
#### explications
Grossièrement, une clé SSH fournit une protection de communication entre une machine (par exemple votre ordinateur) et un serveur distant (Git par exemple).Le but ici est de générer une paire de clé SSH. La première sera privée et restera sur votre machine, tandis que la seconde sera publique. Ainsi il faudra ajouter cette dernière à votre profil Gitlab. Pour plus d'explications, je vous invite à visionner une vidéo qui schématise bien ce concept en cliquant [ici](https://www.youtube.com/watch?v=qixAZdj-I4I). 

#### Commandes

Afin de générer la clé SSH, il faiut entrer une des deux commandes ci-dessous dans votre terminal
```
ssh-keygen -t ed25519 -C "<comment>"

ssh-keygen -t rsa -b 2048 -C "<comment>"

```
Voici ce qui devrait etre affiché : 
![Alt text](./images/compressed.png)


Appuyez sur votre touche Enter 3 fois afin de confirmer le chemin d'accès et de ne pas mettre de mot de passe. Une fois les clés générées, il faut ensuite récupérer la clé publique en vous rendant sur votre dossier .ssh grâce à la commande `cd .shh`.
il vous faudra ensuite entrer toutes les informations sur votre nouvelle clé ssh.
Ensuite entrer ```cat ~/.ssh/id_ed25519.pub``` dans votre terminal. Ceci affiche votre clé SSH publique, copiez la en faisant un clic droit puis copy afin de la copier dans votre clipboard. Allez dans Clé SSH sur Gitlab, cliquez sur "ajouter une clé" et ajoutez votre clé que vous venez de copier. Cette étape étant terminée, vous pouvez désormais cloner votre repository sur votre machine.
## Clone 
Dans votre repository (dépot) sur Gitlab, clonez votre clé SSH et tapez la commande

`git clone elem`, où elem représente l'élément que vous clonez depuis Gitlab.

## synchronisation 

La synchronisation des données entre la machine et le serveur distant nécessite quelques petites commandes peu compliquées.

Tout d'abord, tapez `git pull` pour télécharger le contenu du dépot distant et mettre à jour le dépot local fain de faire correspondre les contenus. Très pratique lorsque plusieurs personnes travaillent sur un même fichier.
Puis sauvergarder les changements effectués (save dans visual studio code).
Ensuite, entrez `git status` pour afficher les changements qui ont été effectués. S'ils vous conviennent, entrez `git commit -am "message"` de sorte à sauvergarder vos modifications.
Enfin, entrez `git push` afin de synchroniser vos modifications avec le contenu du serveur distant.
## taille du fichier
Lors de la documentation, il est imortant de vérifier que les fichiers créés ne soient pas trop volumineux. Cela pourrait ralentir voire empêcher la bonne lecture de votre fichier.
### compression d'image 
Afin de réduire la taille des images que j'ai intégré dans ma documentation, j'ai simplement utilisé ![](https://compressjpeg.com/fr/). Ce site permet de réduire la taille des images de 4 formats différents avec un bon rendement.

## Compression vidéo
Voici le lien d'un compresseur de vidéo en ligne : ![cliquez ici](https://www.freeconvert.com/video-compressor)
 
## logiciel de traitement de texte

Personnellement, j'ai utilisé Visual Studio Code comme logiciel de traitement de texte. J'ai préalablement enregistré ma documentation dans un onglet des dossiers de ma boite virtuelle que j'ai nommé jean.boussier.
Je vous recommande d'utiliser ce logiciel très pratique, doté de son propre terminal dans lequelle je peux entrer les commandes git. 
Pour ouvrir la documentation Git dans Visual Studio,

## gestion de projet
Un grande partie de la gestion de projet dans le cadre de ce coirs repose sur une bonne gestion de la documentation. Une documentation régulière demande de l'investissement mais celle-ci est encore plus chronophage lorsqu'il y a accumulation de retard.
Par ailleurs il y a d'autres facteurs qui permettent de mener à bien le projet. Voici quelques astuces qui pourraient etre utiles  pour une bonne gestion de projet : 
- Se fixer une durée mensuelle de documentation des étapes du projet durant la semains qui s'est écoulée.
- Prendre des photos des étapes importantes du projet. Cela permettra de concevoir une sorte de ligne du temps visuelle qui pourra notament servir de rappel.
- Trouver un bon équilibre entre apprentissage et prototypage. Spécifiquement, dépotans ce cours, je dirais même que l'entreprise d'un prototypage rapide est plus importante. Cette facon d'agir nécessite de prendre des initiatives, (et je pense que c'est une bonne chose).
- Suivre les cours et les formations au maximum pour ne pas être en décalage avec la matière. 
- effectuer les tâches demandées.





