# 5. Dynamique de groupe et projet final
 Ce module retrace notre orientation vers le groupe de projet et la problématique choisie.

## Introduction
### Arbres à problème et à solution
Lors du tout premier cours de PHYS-F-517, notre tuteur, Denis Terwagne, nous a demandé de dessiner un arbre à problème ainsi qu'un arbre à solution. Un arbre à problème illustre une problèmes sous forme d'un tronc, les causes sous la forme de racines (ce sont bien les racines qui sont à l'origine du tronc) et les brancges sous forme de conséquences.
#### arbre à problème

![Alt text](./images/IMG_2452.jpeg)

#### arbre à solution

![Alt text](./images/IMG_2453.jpeg)

J'avais choisi les pertes de eau dans le sol comme problématique parce que celle-ci fait partie une branche très importante de la bioingénieurie : l'agro-écologie.
Je dois dire que mes arbres sont un peu brouillon. Il est tout à fait possible de dessiner ses arbres sur Inskape ou un autre logiciel de dessin. Pour le dessiner je me suis inspiré de mes connaissances aquises au cours d'agronomie, ainsi qu'au [site suivant](https://www.ontario.ca/fr/page/lerosion-du-sol-causes-et-effets)

## "Brainstorming" (séance de remue-méninges) et formation du groupe
La partie projet de groupe a débuté avec une séance "kick-off" (coup-d'envoi), pendant laquelle les projets des années précédentes nous ont été présentés. Etienne de la _Boutique des sciences_ a lié les sciences à la société et nous a illustré comment celles-ci peuvent résoudre des problèmes du quotidien.
### objet choisi
Lors de la Boutique des sciences, nous devions énoncer entre autre des problématiques qui nous affectaient. Ce jour-là j'avais choisi d'aborder le sujet des prix exorbitant des TGV de la SNCF par rapport aux prix dérisoires proposées d'un billet d'avion chez certaines compagnies aériennes. 
Le cours suivant, nous devions apporter un objet qui pourrait répondre à cette problématique. J'ai donc originalement apporté une clé de douze, parce que je me suis dit que la meilleure solution pour pallier ce problème, c'était de construire les voies de chemin de fer et les trains soi-meme.

![Alt text](./images/Untitled.webp)

### formation du groupe (quatuor légendaire)
Nous avions tous amené des objets représentatifs pour le présenter à la classe. Une fois les objets exposés, nous devions discutez des objets qui nous parlaient le plus avec leur propriétaire.
Par le fruit du destin, j'ai donc formé un groupe avec [Lukasssssssss](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/lukas.schieble/-/tree/main/docs?ref_type=heads), [Diane](https://diane-bilgischer-fablab-ulb-enseignements-2023-2-d1bfd7507c373a.gitlab.io/) et [Ana](https://anapatricia-fernandes-fablab-ulb-enseignements-2-d18a5513a59a59.gitlab.io/). 
### Problèmes soulevés en groupe lors du remue-méiniges
Une fois le groupe formé, nous avons encore eu une discussion sur nos objets, puis nous nous sommes mis à la recherche de problématiques en lien avec nos objets. Je tiens à préciser que je ne n'avais pas du tout en tête que mon objet devait être à l'origine d'une ébauche d'une problématique de groupe. 
Voici ce que nous avions trouvé : 

![Alt text](./images/plangroupe.jpg)

 Les thèmes que préferions sont ceux qui comptaient le plus de post-it, c'est-à-dire : 
- comment éveiller les consciences à la surconsommation d'électricité,
- comment analyser le sang à la maison (par rapport à certaines infections, problématique qui est déjà une solution en tant que telle :-/ ).

Au terme de ce cours, nous sommes repartis avec une base, un groupe et un peu d'espoir. Je pense que toutes les activités qui nous ont été proposées pendant ces 2 jours de formation de groupe ont été utiles, parce qu'elles nous ont permis de sortir de notre zone de confort et de nous affirmer en discutant avec des personnes qui n'ont parfois pas les memes idées que vous.
### impression
J'avoue avoir rencontré mes partenaires un peu par hasard,en discutant avec eux lors de la séance, non pas parce que j'étais particulièrement intéressé par leur objet (Lukas avait approté une pièce de 1euro, Diane un livre sur l'architecture et Ana un bout de plastique), mais nous avons été "contraints" de faire un groupe avec les gens avec lesquels ous étions rassemblés. C'est la seule fausse-note que je dois souligner lors de cette activité (qui n'en est pas vraiment une, parce que je me suis retrouvé par chance avec des personnes très sympathiques). Peut-être qu'un effort de communication devra être fourni dans les années à venir.

## Dynamique de groupe
Même si je ne savais pas quoi penser du cours nous expliquant les principes permettant d'avoir une bonne dynamique de groupe, je dois bien avouer que cela a été utile. En effet, lors de nos premières réunion, nous nous sommes étonnament bien entendus (dans les deux sens du terme) et les discussions et débats ont toujours été très fluides. Voici 3 outils qui nous permis de créer cette dynamique. 
### "The wheather" (la météo)
C'est ce principe qui à mes yeux a été le plus important. En effet j'ai senti que ce procédé permettait de nous dégourdir en nous faisant sortir de notre zone de confort et de participer dès l'entame d'une réunion de projet. Cela consiste simplement à raconter à tour de rôle les faits drôles ou marquants que l'on avais vécu la semaine précédente. C'est moi qui animiat la météo (qui est en quelque sorte l'introduction de nos réunions de projet) en faisant des petites blagues pour veiller à ce que tout le monde soit détendu.
### Rôles 
Un des principes de la dynamique de groupe fût aussi de distribuer à chacun un rôle qu'il devait interprêter de manière à ce que la séance se déroule bien : 
- Lukas : l'hôte, charsimatique, sait parfaitement faire la balance entre les moments de rigolades et les moments d'action, où toute blague est incongrue.
- Ana : gestionnaire du temps, qui perçoit le temps comme personne le le perçoit et distribue parfaitement les temps de parole.
- Diane : secrétaire, scribe d'une autre époque, sa plume oscille sur sa feuille aussi vite qu'une onde dans l'ultraviolet.
- Enfin, moi, Jean : animateur météo.
### Vote
Enfin, ce principe de bienséance de réunion de projet est plus courant, mais a tout de même son importance afin que de trouver un terrain d'entente équitable pour chaque décision.
