# Conception Assistée par Ordinateur (CAO)

Dans ce module, je présenterai les outils qui m'ont permis de dessiner mon objet 3D (voir module 3 pour l'impression)

##  Choix du logiciel : OpenSCAD
La première consiste à choisir un logiciel de modélisation 3D afin de dessiner notre objet 3D J'ai donc choisi OpenSCAD qui est un logiciel libre de modélisation paramétrique,  parce qu'il permet une paramétrisation simple de l'objet dessiné.
Si vous cherchez des informations concernant le code, cliquez [ici](https://openscad.org/cheatsheet/index.html) pour accéder à la documentation et aux différentes commandes d'OpenSCAD

## Compliant mechanism (pas de traduction adéquate en français) : introduction
Le but de ce module était de modéliser un compliant mechanism. Voici le [lien](https://www.youtube.com/watch?v=97t7Xj_iBv0) d'une video, suggérée par notre tuteur, monsieur Terwagne, qui vous permettra de mieux appréhender le concept de compliant mechanism.
## mon compliant mechanism
N'ayant pas d'idée géniale, j'ai décidé de faire un petite pince toute simple déformable.
<div class="sketchfab-embed-wrapper"> <iframe title="STL Pince" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" allow="autoplay; fullscreen; xr-spatial-tracking" xr-spatial-tracking execution-while-out-of-viewport execution-while-not-rendered web-share src="https://sketchfab.com/models/eff33db3d86046fba71220aad33e4de2/embed"> </iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/3d-models/stl-pince-eff33db3d86046fba71220aad33e4de2?utm_medium=embed&utm_campaign=share-popup&utm_content=eff33db3d86046fba71220aad33e4de2" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;"> STL Pince </a> by <a href="https://sketchfab.com/jbou0004?utm_medium=embed&utm_campaign=share-popup&utm_content=eff33db3d86046fba71220aad33e4de2" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;"> jbou0004 </a> on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=eff33db3d86046fba71220aad33e4de2" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div>
J'ai chosi de mettre une licence CC BY afin que tout lem monde puisse avoir accès à mon travail, du moment que je suis mentionné.

## Autre idée : crochet
 Ici l'idée était de faire, avec mon binome Christopher Bilba, un crochet qui puisse etre attaché à un ressort. Pour ma part, je me suis occupé de la conception du crochet. Bien qu'il ne soit lui-meme pas vraiment un compliant mechanism au vu de sa faible déformabilité, le tout peut etre vu comme un tel objet. C'est ce crochet que j'ai imprimer en 3D.

### code 

J'ai entré le code ci-dessous, auquel j'ai apporté quelques petites modifications paramétriques.
```
// From http://www.thingiverse.com/thing:102974

wall_hole_d = 5;
wall_hole_offset = 10;

/* [Door parameters] */
// Door thickness (inner distance between front and back of door hook)
clip_depth = 37;
// Thickness of top and overhang (make sure it fits gap between door and frame)
top_and_back_thickness = 2;

/* [Hook parameters] */
// Width of entire hook
width = 30;
// Thickness of front piece and hanger hooks
hook_and_front_thickness = 4;
// Length of overhang on back side of the door
clip_length = 55;


number_of_hooks = 1;
// Distance between top of door and first hanger hook
first_hook_offset = 35;  // rounded: 60
// Distance between successive hanger hooks (if more than one)
hook_distance = 60;  // rounded: 60
// Radius of hanger hook curve
hook_radius = 15;

hook_style = "rounded"; // [simple:Simple,rounded:Rounded]

/* [Advanced] */

// Angle back of clip inwards (towards door), to help with friction; don't go crazy with this or you'll put too much stress on clip
clip_angle = 1;
// Add support buttress on hooks (except last); might help reduce flex, but looks a bit uglier (IMO)
inner_hook_buttress = 0; // [0:No,1:Yes]

double_sided = 1; // [0:No,1:Yes]

/* [Hidden] */

$fn = 72;
thickness = hook_and_front_thickness;
top_thickness = top_and_back_thickness;
back_thickness = double_sided ? thickness : top_thickness;

module hook_base() {
  difference() {
    cylinder(h=width, r=hook_radius+thickness);
    translate([0,0,-1])
      cylinder(h=width+2, r=hook_radius);
    translate([-hook_radius-thickness-1, 0, -1]) 
      cube([2*(hook_radius+thickness+1), hook_radius+thickness+1, width+2]); 
  }
}

module simple_hook() {
  translate([hook_radius+thickness, 0, 0]) union() {
    hook_base();
    translate([hook_radius+thickness/2, 0, 0])
      cylinder(h=width, r=thickness/2, $fn=18);
  }
}

module rounded_hook() {
  translate([0, -hook_radius-thickness, 0]) difference() {
    intersection() {
      cube([2*(hook_radius+thickness), 1.5*hook_radius+thickness+1, width]);
      translate([0, 0, width/2]) rotate([0, 90, 0]) cylinder(h=2*(hook_radius+thickness), r=1.5*hook_radius+thickness);
      translate([hook_radius+thickness, hook_radius+thickness, -1]) cylinder(r=hook_radius+thickness, h=width+2);
    }
    translate([hook_radius+thickness, hook_radius+thickness, -1]) cylinder(r=hook_radius, h=width+2);
    translate([-1, hook_radius+thickness, -1]) cube([hook_radius+thickness+2, hook_radius, width+2]);
  }
}

module hook() {
  if (hook_style == "simple") {
    simple_hook();
  } else if (hook_style == "rounded") {
    rounded_hook();
  }
}

module clip() {
  front_length = first_hook_offset + (number_of_hooks - 1) * hook_distance;
  back_length = double_sided ? front_length : clip_length;
  union() {
    translate([-clip_depth-back_thickness, 0, 0]) rotate([0, 0, clip_angle]) translate([0, -back_length, 0])
      cube([back_thickness, back_length, width]);
    translate([-clip_depth-back_thickness, 0, 0])
      cube([clip_depth + back_thickness + thickness, top_thickness, width]);
    translate([0, -front_length, 0])
      cube([thickness, front_length, width]);
  }
}

butt_theta = acos(hook_radius/(hook_radius+thickness));
module each_hook(i) {
  translate([0, -first_hook_offset-i*hook_distance+.1, 0]) union() {
    hook();
    if ((i < number_of_hooks-1) && (inner_hook_buttress != 0)) {
      translate([0,-(hook_radius+thickness)*tan(butt_theta),0]) rotate([0, 0, butt_theta]) translate([0, -thickness, 0]) cube([thickness*(1+1/cos(butt_theta)),thickness,width]);
    }
  }
}

module door_hook() {
  clip();
  for (i = [0:(number_of_hooks-1)]) {
    each_hook(i);
    if (double_sided) {
      translate([-clip_depth, 0, width])
      rotate([0, 0, clip_angle])
      rotate([0, 180, 0])
      each_hook(i);
    }
  }
}

module wall_hook() {
  difference() {
    union() {
      front_length = first_hook_offset + (number_of_hooks - 1) * hook_distance;
      translate([0, -front_length, 0]) cube([thickness, front_length, width]);
      for (i = [0:(number_of_hooks-1)]) {
        each_hook(i);
      }
    }
    translate([-1, -wall_hole_offset, width/2]) rotate([0, 90, 0]) cylinder(d=wall_hole_d, h=thickness+2);
    translate([thickness/2+.1, -wall_hole_offset, width/2]) rotate([0, 90, 0]) cylinder(d1=wall_hole_d, d2=wall_hole_d*2, h=thickness/2);
    
  }
}

//door_hook();
wall_hook();
```
J'ai choisi ce code parce que je l'ai trouvé très simple au niveau de la paramétrisation (taille du crochet, dela courbure,...). Cependant, j'admet que la rédaction de la fonction n'a pas du etre une simple formalité.
Voici à quoi ressemble mon objet : 
![Alt text](./images/Capture%20d'écran%202023-10-24%20105527.png)
![Alt text](./images/corchet%20trou.png)


