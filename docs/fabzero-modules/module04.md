# 4. Prototypage électronique

J'ai choisi le module "electronic prototyping". J'ai choisi ce module parce que jes suis très intéressé par l'électronique, malgré le fait que ce domaine est encore très nébuleux pour moi. Meme si ce n'est pas vraiment ce à quoi je m'attendais (j'aurais aimé que le professeur nous explique plus en détails le fonctionnement des microcontrolleurs et de leurs composantes), j'ai pu explorer l'univers des microcontrolleurs et avoir une idée un peu plus précise de leur fonctionnement.

## Qu'est-ce qu'est Arduino 
Arduino est une entreprise qui conçoit et qui crée du matériel électronique (dont des microcontrôleurs), programmable en langage C ou C++ assez simplement par un logiciel, surtout d'utilité pour les non-ingénieurs.Les microcontrôleurs eux sont des circuits intégrés qui rassemblent les éléments essentiels d'un ordinateur.
Pour une meilleure compréhension de ce sujet, je vous conseille vivement de regarder une vidéo qui explique cela très bien en cliquant [ici](https://www.youtube.com/watch?v=U9T0fjFyVow). 

### téléchargement
Personnellement, j'ai téléchargé la dernière version d'Arduino IDE (2.2.1) tout simplement parce que le tuteur l'utilisait lors de notre formation. Voici le lien pour télécharger [Arduino](https://www.arduino.cc/en/software)

## Premier exercice : programmation d'un capteur de température et d'humidité
Ma première mission a été de programmer un capteur de température et de pression. 
### montage
![Alt text](./images/IMG_2429.jpeg)
### capteur 
![Alt text](./images/IMG_2437.jpeg)
Cliquez [ici](https://cdn.sparkfun.com/assets/8/a/1/5/0/DHT20.pdf) pour avoir accès à la fiche du capteur.
### code 
Le principe général de programmation Arduino est relativement simple : preùièrement il faut introduire un série de définition grâce à la commande `voice setup`. Secondement il faut entrer les instructions nécessaires au programme sous la commande `voice loop`. Voici le programme que j'ai utilisé : 

```#include "DHT20.h"

DHT20 DHT(&Wire);


void setup()
{
  Wire.begin();
  Wire.setSDA(0);
  Wire.setSCL(1); // brancher les entrées SDA et SCL du capteur aux pins 0 et 1

  Wire.begin();
  //  ESP32 default pins 21 22

  Serial.begin(115200);
  Serial.println("Humidity, Temperature");
}


void loop()
{
  if (millis() - DHT.lastRead() >= 1000)
  {
    //  note no error checking
    DHT.read();
    Serial.print(DHT.getHumidity(), 1);
    Serial.print(", ");
    Serial.println(DHT.getTemperature(), 1);
  }
}

```
Evidemment, il faut impérativement brancher les fils électriques en série entre les branches du capteur et les pins (trous) adéquats de la board.
### Connection du PC au dispositif électronique
Avant de lancer la courbe de représentation, quelques petits paramétrages sont nécessaires.
 Tout d'abord, j'ai entré l'URL suivant dans les préférences :
 `https://github.com/earlephilhower/arduino-pico/releases/download/global/package_rp2040_index.json`
 ![Alt text](./images/Capture%20d'écran%202023-11-22%20120904-min.png)
 
 Ensuite, j'ai sélectionné le bon board, c'est-à-dire le microcontroleur que vous avez connecté à votre ordinateur via la cable USB. 

Enfin, il est aussi nécessaire de télécharger des éléments de la librairie de sorte que le programme soit exécuté correctement. Par exemple, pour le programme du capteur d'humidité et de température, j'ai téléchargé dans la bibliothèque Arduino DHT 20.h, car ce programme inclut DHT 20.h (voir image ci-dessous).

![Alt text](./images/Capture%20d'écran%202023-11-22%20110645-min.png)

### problème rencontré : board déffectueux
L'une des grandes difficultés que j'ai rencontré (non seulement moi mais aussi la classe en général), fut la reconnaissance du board par le port USB de mon ordinateur. 
Heureusement, un de mes collègues a fini par trouver une solution. 
Voici les différentes étapes : 

D'abord, appuyez sur le bouton boot de votre microcontroleur pendant que vous branchez l'entrée USB à l'ordinateur (la reconnaissance de l'USB va etre affiché une première fois sur l'ordinateur, voir ci-dessous).  Sélectionnez le board Rasberry Pico dans Arduino ( voir en haut à gauche de l'image)

![Alt text](./images/Capture%20d'écran%202023-11-22%20114055-min.png)

![Alt text](./images/Capture%20d'écran%202023-11-22%20114142-min.png)

Lors de l'éxecution du programme, vous constaterez que celui est en train de charger, mais que le port USB n'est plus détecté. Pas de panique : Arduino va automatiquement changer de board pour passer au generic RP2040. 
Exécutez une nouvelle fois le programme et vous verrez cette fois-ci que le port USB est connecté à l'ordinateur (comme indiqué ci-dessous)

![Alt text](./images/Capture%20d'écran%202023-11-22%20114216-min.png)

## Interprétation du tracé (plotter)
 En ouvrant mon tracé.(terme que j'utiliserai pour traduire plotter qui est un anglicisme), j'observe un plan avec deux courbes. J'ai programmé le SDA en x (représentation du temps qui s'écoule en direct) et le SCL en y (représente les intensités de l'humidité et la température enregistrées). La courbe bleur représente l'humidité en fonction du temps, tandis que la courbe rouge représente la température en fontion du temps.

 Afin d'interpéter ce graphe et de vérifier qu'il ne me délivre pas d'informations indésirables, j'ai considéré 3 situations distinctes.

### Tracé "normal"

![Alt text](./images/normal_plotter-min.png)

Voici tout d'abord le tracé qui correspond à la situation ou je n'ai exercée aucune influences sur le détecteur. L'humidité et la température sont à peu près constantes, ce qui est logique.
### Tracés caractéristiques

![Alt text](./images/souffle_capt-min.png)

Ici, j'ai soufflé sur le détecteur. On voit bien un pic au niveau de l'humidité. Logique : l'air qui sort de ma bouche est humide et chaud (d'ou la légère augmentation de la tempéraure). On peut donc en conclure que je ne me suis pas trompé dans la programmation, que j'ai utilisé le bon code. 

Voici un autre exemple, cette fois avec mon sèche-cheveux (on s'attend à un air sec et chaud, ce qui est corrélé par le tracé)

![Alt text](./images/hairdryer-min.png)

 ## Autre application : détecteur de CO2
 Afin d'aller plus loin, j'ai programmé avec mon collègue Lukas Schieble un détecteur de CO2 équipé d'une LED. Nous avons paramétrisé le programme de telle manière que la LED clignote d'une certaine manière sous certaines conditions. 
 ### montage
 ![Alt text](./images/IMG_2364.jpeg)
 ### fiche 
 Cliquez [ici](https://www.tinytronics.nl/shop/en/sensors/air/gas/memsfrontier-mtp40-f-co2-sensor) pour plus d'informations. XX image et expliaction fiche capteur
 ### code
 ```//    FILE: MTP40F_getCO2_plotter.ino
//  AUTHOR: Rob Tillaart
// PURPOSE: demo of MTP40F library
//     URL: https://github.com/RobTillaart/MTP40F
//
//  any board that support two or more hardware serial ports
//  Serial and Serial1, e.g. for MEGA, LEONARDO, MICRO, ESP32, ESP8266
//  Uno, Nano or Mini will fail to compile.
//
//  SCHEMA example SoftwareSerial sws(7, 6);
//              UNO                     MTP40F
//        +---------------+        +---------------+
//        |               |        |               |
//        |          +5V  |--------| 1 VIN         |
//        |          GND  |--------| 2 GND         |
//        |               |        | 3             |
//        |               |        | 4             |
//        |               |        |               |
//        |               |        | 5             |
//        |         RX 6  |--------| 6 TX          |
//        |         TX 7  |--------| 7 RX          |
//        |               |        | 8             |
//        |               |        | 9             |
//        |               |        |               |
//        +---------------+        +---------------+
//

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#define PIN        23
#define NUMPIXELS 1 
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
#define DELAYVAL 500 
#include "MTP40F.h"
#include "SoftwareSerial.h"

SoftwareSerial sws(7, 6);

MTP40F mtp(&sws);       // use hardware Serial1 for MTP40C sensor


void setup()
{
  Serial.begin(115200);
  // Serial.println(__FILE__);
  // Serial.print("MTP40F_LIB_VERSION:\t");
  // Serial.println(MTP40F_LIB_VERSION);
  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
  #endif
  pixels.begin();
  sws.begin(9600);
  mtp.begin();
}


void loop()
{
  pixels.clear();
  if (millis() - mtp.lastRead() >= 2500)
  {
     for(int i=0; i<NUMPIXELS; i++) {
      if (mtp.getGasConcentration() > 1500){
        pixels.setPixelColor(i, pixels.Color(150, 0, 0));

        pixels.show();   // Send the updated pixel colors to the hardware.

        delay(DELAYVAL);
      }
      else {
        pixels.setPixelColor(i, pixels.Color(0, 150, 0));

        pixels.show();   // Send the updated pixel colors to the hardware.

        delay(DELAYVAL);
      }
  }
}
}


//  -- END OF FILE --```
 
Ensuite nous avons modifié le code pour le fun et avons programmé l'appareil de sorte à faire une machine un peu "disco".

``` // NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// Released under the GPLv3 license to match the rest of the
// Adafruit NeoPixel library

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#define PIN        23
#define NUMPIXELS 1 
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
#define DELAYVAL 100 
void setup() {
  // These lines are specifically to support the Adafruit Trinket 5V 16 MHz.
  // Any other board, you can remove this part (but no harm leaving it):
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  // END of Trinket-specific code.

  pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
}

void loop() {
  pixels.clear(); // Set all pixel colors to 'off'

  // The first NeoPixel in a strand is #0, second is 1, all the way up
  // to the count of pixels minus one.
  for(int i=0; i<NUMPIXELS; i++) { // For each pixel...

    // pixels.Color() takes RGB values, from 0,0,0 up to 255,255,255
    // Here we're using a moderately bright green color:
    pixels.setPixelColor(i, pixels.Color(0, 150, 0));

    pixels.show();   // Send the updated pixel colors to the hardware.

    delay(DELAYVAL); // Pause before next pass through loop
    pixels.setPixelColor(i, pixels.Color(150, 0, 0));

    pixels.show();   // Send the updated pixel colors to the hardware.

    delay(DELAYVAL); // Pause before next pass through loop
    pixels.setPixelColor(i, pixels.Color(0, 0, 150));

    pixels.show();   // Send the updated pixel colors to the hardware.

    delay(DELAYVAL); // Pause before next pass through loop
    pixels.setPixelColor(i, pixels.Color(150, 150, 150));

    pixels.show();   // Send the updated pixel colors to the hardware.

    delay(DELAYVAL); // Pause before next pass through loop
  }
}
```
 
