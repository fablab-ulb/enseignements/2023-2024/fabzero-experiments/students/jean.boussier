# 3. Impression 3D
Dans ce module je présente les différentes étapes de l'impression de mon objet 3D.
## imprimante
L'imprimante 3D utilisée est celle proposée au FabLab : la Prusa i3 MK3.
![Alt text](./images/imprimante.jpg)
## slicer
"Le slicer, également appelé logiciel de découpage en tranches, est un logiciel utilisé dans la majorité des processus d'impression 3D pour la conversion d'un modèle d'objet 3D en instructions spécifiques pour l'imprimante." L'avantage d'utiliser une imprimante Prusa est qu'elle possède son propre slicer : PrusaSlicer (2.6.1, c'est cette version que j'ai utilisée)
### exportation format .stl
Avant toute chose, il est important d'exporter votre représentation 3D sous forlat STL pour qu'il puisse être imprimable, en calculant d'abord le rendu de l'image (render en anglais).

### temps d'impression 
 Il est bien sur possible de réduire le temps d'impression initialement prévu par le slicer. Une technique consiste naturellement à diminuer les dimensions de votre objet. En effet, c'est cette démarche que j'ai suivie pour diminuer le temps d'impression de mon objet (de 45 minutes à 25 minutes), cependant il existe d'autes méthode (retrait du support, type d'impression souhaitée, etc.)

### choix des paramètres impression
Une fois que le temps d'impression vous convient, vous devez choir les bons paramètres d'impression, c'est-à-dire qu'il faut choisir la bonne imprimante et la bonne taille de fil PLA. Il suffit de sélectionner les options représentées ci-dessous : 

![Alt text](./images/firstPrusa1.PNG)

Ci-dessus voici l'interface de Prusa slicer

![Alt text](./images/secondPrusa2.PNG)

Ici j'ai séléctionné l'imprimante avec le bon format des buses (0,4mm) des imprantes employées au Fablab.

![Alt text](./images/thridPrusa3.PNG)

Ici je n'ai rien d'autre que de suivre les précieux conseils de John, et j'ai simplement choici le matériau utilisé (le PLA) et le paramètre Prusament PLA (je ne sais pas trop ce que c'est mais j'ai obéi).

![Alt text](./images/forthPrusa5.PNG)

Si vous avez choisi la bonne imprimante, vous derviez voir le plateau ci-dessus apparaitre sur votre slicer.
Remarque importante : normalement, vous devrez aussi choisir votre vitesse d'impression et votre taille de fil. Intuitivement, il est facile de supposer que plus votre fil sera fin, plus l'impression sera lente. A l'inverse, plus le fil sera épais, plus l'impression sera rapide. Quand à la vitesse que vous devez choisir, tout dépend de la précision des contours que vous souhaitez.
N'oubliez pas non plus d'appuyer sur l'icône en haut à gauche et de jouer avec la forme qui va apparaître pour savoir si votre objet virtuel est correctement orienté.
Finalement, si vous débutez en impression 3D, je vous recommande de d'abord imprimer avec support. En faisant cela, l'imprimante aura un point de repère et l'impression se fera de facon plus stable.

### format STL
<div class="sketchfab-embed-wrapper"> <iframe title="crochetSTL" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" allow="autoplay; fullscreen; xr-spatial-tracking" xr-spatial-tracking execution-while-out-of-viewport execution-while-not-rendered web-share src="https://sketchfab.com/models/f767bd2519704396b18b883c7de71bdb/embed"> </iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/3d-models/crochetstl-f767bd2519704396b18b883c7de71bdb?utm_medium=embed&utm_campaign=share-popup&utm_content=f767bd2519704396b18b883c7de71bdb" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;"> crochetSTL </a> by <a href="https://sketchfab.com/jbou0004?utm_medium=embed&utm_campaign=share-popup&utm_content=f767bd2519704396b18b883c7de71bdb" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;"> jbou0004 </a> on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=f767bd2519704396b18b883c7de71bdb" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div>

## résultat 
![Alt text](./images/IMG_2450.jpeg)
![Alt text](./images/IMG_2451.jpeg)





