# Présentation



## A propos de moi
![image 1](88331206_509602553029955_8712357369142575104_n.jpg)


Je m'appelle Jean, et je suis étudiant en troisième bachelier bioingénieur à l'ULB.

Vous etes actuellement sur mon site.

## Scolarité

Je suis né le 13 novembre 2002 dans la petite borugade de Paris. Après plusieurs déménagements mes parents trouvèrent définitivement refuge à Bruxelles. C'est là que j'ai fait la quasi totalité de ma scolarité, dans la joie et la bonne humeur. Aux termes de mes humanités, j'étais un peu désorienté. Cependant j'ai entrepris une entrée à l'école polytechnique de l'ULB. Hélas, j'ai échoué de relativement peu du fait de mes lacunes en géométrie. Par chance, une connaissance m'avait parlé peu de temps avant de la faculté de bioingénieur à l'ULB. Je me suis donc lancé dans cette aventure, et je n'ai (presque) jamais regretté.
## Hobbies 
J'aime écouter et jouer de la musique (je fais de la basse et de la batterie). De plus, mon papa m'a transmis le gout du foot quand j'étais petit, et depuis j'adore le pratiquer et le regarder à la TV (mon équipe préférée c'est Anderlecht).

## Job étudiant
Actuellement, je suis vendeur d'appareil électronique chez Lab9.
